
package CompraVentaAuto.Herramienta;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class EnvioEmail {
    
    
    private String _cuentaEmisor;
    private String _claveEmisor;
    
    public EnvioEmail(String cuentaEmisor, String clave)
    {
        this._cuentaEmisor = cuentaEmisor;
        this._claveEmisor = clave;
    }
    
    public String enviarByGMail(String destinatario, String asunto, String cuerpo) 
    {
        String msgOut = "1";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", _cuentaEmisor);
        props.put("mail.smtp.clave", _claveEmisor);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(_cuentaEmisor));
            message.addRecipients(Message.RecipientType.TO, destinatario);   //Se podrían añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", this._cuentaEmisor, this._claveEmisor);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            msgOut = "Error: " + me.getMessage();
        }
        
        return msgOut;
    }
}
