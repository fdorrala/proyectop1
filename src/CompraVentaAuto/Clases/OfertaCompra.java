
package CompraVentaAuto.Clases;

import CompraVentaAuto.Herramienta.ConvertFileToClass;
import CompraVentaAuto.Herramienta.ProcesadorArchivo;
import java.io.File;
import java.util.ArrayList;

public class OfertaCompra implements ConvertFileToClass{

   
    private final String ARCHIVO = "OFERTACOMPRA.CSV";
    private String _directorio = "";
    
     private Comprador _ofertante;
     private Vehiculo _vehOferta;
     private double _valor;
     private boolean _aceptada;
     
     private ProcesadorArchivo _procesadorFile;
     
     public OfertaCompra()
     {
         this._ofertante = null;
         this._vehOferta = null;
         this._valor = 0;
         this._directorio = new File("").getAbsolutePath()+ "\\ARCHIVOS";
         this._procesadorFile = new ProcesadorArchivo(_directorio, ARCHIVO, this);
     }
     public OfertaCompra(Comprador ofertante, Vehiculo vehiculo, double valor)
     {
         this._ofertante = ofertante;
         this._vehOferta = vehiculo;
         this._valor = valor;
         _directorio = new File(".").getAbsolutePath();
         this._procesadorFile = new ProcesadorArchivo(_directorio, ARCHIVO, this);
     }
    
     
    public static ArrayList<OfertaCompra> GetOfertasxPlaca(ArrayList<OfertaCompra> ofertas, String placa)
    {
       ArrayList<OfertaCompra> ofertasPlaca = null;
       
       for(int i = 0;i<ofertas.size();i++)
       {
           OfertaCompra temp = ofertas.get(i);
           if(temp.getVehiculo().getPlaca().equalsIgnoreCase(placa)
              && !temp._aceptada)
           {
               if(ofertasPlaca==null)
                   ofertasPlaca = new ArrayList<>();
               
               ofertasPlaca.add(temp);
           }
       }
       
       return ofertasPlaca;
    }
    
    
    @Override
    public ArrayList GetArrayListVacioInstancia() {
        return new ArrayList<OfertaCompra>();
    }

    @Override
    public Object ConvertLineaToClase(String lineaArchivo) {
        OfertaCompra oferta = null;
        
        if(lineaArchivo!=null && !lineaArchivo.trim().equals(""))
        {
            String[] datos = lineaArchivo.split(";");
            oferta = new OfertaCompra();
            oferta.setOfertante(new Comprador());
            oferta.setVehiculo(new Vehiculo());
            oferta.getOfertante().setNombres(datos[0]);
            oferta.getOfertante().setApellidos(datos[1]);
            oferta.getOfertante().setEmail(datos[2]);
            oferta.getVehiculo().setTipo(datos[3]);
            oferta.getVehiculo().setPlaca(datos[4]);
            oferta.getVehiculo().setMarca(datos[5]);
            oferta.getVehiculo().setModelo(datos[6]);
            oferta.getVehiculo().setPrecio(Double.parseDouble(datos[7]));
            oferta.setValor(Double.parseDouble(datos[8]));
            oferta.setAceptada(Boolean.parseBoolean(datos[9]));
        }
        return oferta;
    }

    @Override
    public ArrayList GetListArchivoDBClase() {
        ArrayList<OfertaCompra> lista = null;
        lista = _procesadorFile.GetListadoClases();
        if(lista==null)
            lista = new ArrayList<>();
        return lista;
    }

    @Override
    public String Registrar() {
        String msgOut = null;
        try
        {
            String registro= this.getOfertante()._nombres + ";" + this.getOfertante()._apellidos + ";" + this.getOfertante()._email + ";" +
                    this.getVehiculo().getTipo() + ";" + this.getVehiculo().getPlaca() + ";" + this.getVehiculo().getMarca() +";" + 
                    this.getVehiculo().getModelo() + ";" +  this.getVehiculo().getPrecio() + ";" + this.getValor() + ";0";
            
            msgOut = this._procesadorFile.EscribirLinea(registro);
        }
        catch(Exception ex)
        {
            msgOut = "Error: No se pudo agregar la nueva oferta inténtelo de nuevo";
        }
                
        return msgOut;
    }
    public String generarAsuntoAcepta()
    {
        String asunto;
        
        asunto = "Aceptación de oferta vehiculo " + this.getVehiculo().getMarca() + " " +
                    this.getVehiculo().getModelo();
        
        return asunto;
    }
    public String generarCuerpoMailAcepta()
    {
        String bodyMail = "";
        
        bodyMail = "Se ha aceptado la oferta del vehículo:\n";
        bodyMail += "Tipo Vehiculo: " + this.getVehiculo().getTipo() + "\n";
        bodyMail += "Placa: " + this.getVehiculo().getPlaca()+ "\n";
        bodyMail += "Modelo: " + this.getVehiculo().getModelo()+ "\n";
        bodyMail += "Precio: $" + this.getVehiculo().getPrecio()+ "\n";
        bodyMail += "Valor ofertado: $" + this._valor + "\n";
        
        return bodyMail;
    }
    
    
    public static String ActualizarBaseOfertas(ArrayList<OfertaCompra> ofertas)
    {
        String msgOut = null;
        String[] lineas = new String[ofertas.size()]; 
        String directorio = new File("").getAbsolutePath() + "\\ARCHIVOS";
        ProcesadorArchivo procesador = new ProcesadorArchivo(directorio, "OFERTACOMPRA.CSV", null);
        try
        {
            for(int i=0;i<ofertas.size();i++)
            {
                OfertaCompra ofer = ofertas.get(i);
                lineas[i] = ofer.getOfertante()._nombres + ";" + ofer.getOfertante()._apellidos + ";" + ofer.getOfertante()._email + ";" +
                    ofer.getVehiculo().getPlaca() + ";" + ofer.getVehiculo().getModelo() + ";" + ofer.getVehiculo().getPrecio() + ";" + 
                    ofer.getValor() + ";" + (ofer.isAceptada()?"1":"0");
            }
            msgOut = procesador.EscribirTodoArchivo(lineas);
        }
        catch(Exception ex)
        {
            msgOut = "Error: No se pudo agregar la nueva oferta inténtelo de nuevo";
        }
                
        return msgOut;
    }
    
    

    public Comprador getOfertante() {
        return _ofertante;
    }

    public void setOfertante(Comprador ofertante) {
        this._ofertante = ofertante;
    }

    public Vehiculo getVehiculo() {
        return _vehOferta;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this._vehOferta = vehiculo;
    }

    public double getValor() {
        return _valor;
    }

    public void setValor(double valor) {
        this._valor = valor;
    }
    
    public boolean isAceptada() {
        return _aceptada;
    }

    public void setAceptada(boolean _aceptada) {
        this._aceptada = _aceptada;
    }
}
