
package CompraVentaAuto;

import CompraVentaAuto.Clases.Comprador;
import CompraVentaAuto.Clases.OfertaCompra;
import CompraVentaAuto.Clases.PersonaVtaCmp;
import CompraVentaAuto.Clases.Vehiculo;
import CompraVentaAuto.Clases.Vendedor;
import CompraVentaAuto.Interfaz.OpcionMenu;
import CompraVentaAuto.Interfaz.PantallaConsola;
import java.util.ArrayList;


public class Main {


    public static void main(String[] args) 
    {
        ArrayList<Comprador> lstCompradores = new Comprador().GetListArchivoDBClase();
        ArrayList<Vendedor> lstVendedores = new Vendedor().GetListArchivoDBClase();
        ArrayList<Vehiculo> lstVehiculos = new Vehiculo().GetListArchivoDBClase();
        ArrayList<OfertaCompra> lstOferta = new OfertaCompra().GetListArchivoDBClase();
        
        PantallaConsola pantalla = new PantallaConsola("projectpoo2020@gmail.com","pro",
                            lstCompradores,lstVendedores,lstVehiculos,lstOferta);
       
        OpcionMenu opcionSele  = new OpcionMenu();
        opcionSele.setMenu(OpcionMenu.MENU_PRINCIPAL);
        opcionSele.setOpcionMenu(0);
        do
        {
            
            opcionSele = pantalla.EjecutarOpcion(opcionSele.getMenu(), opcionSele.getOpcionMenu());
        }while(opcionSele!=null);
        
    }
    
}
