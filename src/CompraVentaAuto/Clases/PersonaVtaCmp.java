
package CompraVentaAuto.Clases;
import CompraVentaAuto.Herramienta.CodificadorSHA;
import CompraVentaAuto.Herramienta.ConvertFileToClass;
import CompraVentaAuto.Herramienta.ProcesadorArchivo;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public abstract class PersonaVtaCmp implements ConvertFileToClass{

    public static final int TIPO_COMPRADOR = 1;
    public static final int TIPO_VENDEDOR = 2;
    
    protected String _nombres;
    protected String _apellidos;
    protected String _email;
    protected String _organizacion;
    protected String _usuario;
    protected String _clave;
    protected int _tipoPersona;
    protected ProcesadorArchivo _procesadorFile;
    
    
    public static PersonaVtaCmp EjecLogin(ArrayList<PersonaVtaCmp> personas,String usuario, String clave, int tipo) throws Exception
    {
        PersonaVtaCmp buscado = null;
        for(int i=0;i<personas.size();i++)
        {
            PersonaVtaCmp p = personas.get(i);
            if(p._tipoPersona== tipo
               && usuario.equalsIgnoreCase(p._usuario))
            {
                CodificadorSHA codificador = new CodificadorSHA(clave);
                try {
                    if(p.getClave().equals(codificador.getCodificado()))
                        buscado = p;
                } catch (NoSuchAlgorithmException ex) {
                    throw new Exception("Error: Hubo un error al comparar las contraseñas.");
                }
            }
        }
        return buscado;
    }
    
    
    public String Registrar()
    {
        String msgOut = null;
        CodificadorSHA codificador = new CodificadorSHA(this._clave);
        try
        {
            String registro  = this._nombres + ";" + this._apellidos + ";" + 
                    this._organizacion + ";" + this._email + ";" + this._usuario + ";" + 
                    codificador.getCodificado();
            
            msgOut = this._procesadorFile.EscribirLinea(registro);
        }
        catch(NoSuchAlgorithmException ex)
        {
            msgOut ="Error: No se encontró el algoritmo de codificación.";
        }
                
        return msgOut;
    }
    
    public static String ExistePersona(ArrayList personas,PersonaVtaCmp buscada)
    {
        String msgOut = null;
        
        if(personas!=null)
        {
            for(int i=0;i<personas.size();i++)
            {
                PersonaVtaCmp p = (PersonaVtaCmp)personas.get(i);
                if(p.getNombres().equalsIgnoreCase(buscada.getNombres().trim())
                   && p.getApellidos().equalsIgnoreCase(buscada.getApellidos().trim()))
                {
                    msgOut = "El nombre y apellido ya se encuentra registrado.";
                    break;
                }
                else if(p.getEmail().equalsIgnoreCase(buscada.getEmail().trim()))
                {
                    msgOut = "El correo electrónico ya se encuentra asignado a otra persona.";
                    break;
                }
                else if(p.getUsuario().equalsIgnoreCase(buscada.getUsuario().trim()))
                {
                    msgOut = "El usuario ya se encuentra asignado a otra persona.";
                    break;
                }
            }
        }
        
        return msgOut;
    }
    
    public String getNombres() {
        return _nombres;
    }

    public void setNombres(String _nombres) {
        this._nombres = _nombres;
    }

    public String getApellidos() {
        return _apellidos;
    }

    public void setApellidos(String _apellidos) {
        this._apellidos = _apellidos;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getOrganizacion() {
        return _organizacion;
    }

    public void setOrganizacion(String _organizacion) {
        this._organizacion = _organizacion;
    }

    public String getUsuario() {
        return _usuario;
    }

    public void setUsuario(String _usuario) {
        this._usuario = _usuario;
    }

    public String getClave() {
        return _clave;
    }

    public void setClave(String _clave) {
        this._clave = _clave;
    }
    
}
