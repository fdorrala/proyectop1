/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CompraVentaAuto.Clases;

import CompraVentaAuto.Herramienta.CodificadorSHA;
import CompraVentaAuto.Herramienta.ProcesadorArchivo;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class Comprador extends PersonaVtaCmp{

    private final String ARCHIVO = "COMPRADOR.CSV";
    private String _directorio = "";
    
    
    public Comprador()
    {
        this._directorio = new File("").getAbsolutePath()+ "\\ARCHIVOS";
        this._tipoPersona = super.TIPO_COMPRADOR;
        this._procesadorFile = new ProcesadorArchivo(_directorio, ARCHIVO, this);
        
    }


    @Override
    public Object ConvertLineaToClase(String lineaArchivo) {
        Comprador cmp = null;
        
        if(lineaArchivo!=null && !lineaArchivo.trim().equals(""))
        {
            String[] datos = lineaArchivo.split(";");
            cmp = new Comprador();
            cmp._nombres = datos[0];
            cmp._apellidos = datos[1];
            cmp._organizacion = datos[2];
            cmp._email = datos[3];
            cmp._usuario = datos[4];
            cmp._clave = datos[5];
        }
        return cmp;
    }

    @Override
    public ArrayList GetArrayListVacioInstancia() {
        return new ArrayList<Comprador>();
    }

    @Override
    public ArrayList GetListArchivoDBClase() {
        ArrayList<Comprador> lista = null;
        lista = _procesadorFile.GetListadoClases();
        if(lista==null)
            lista = new ArrayList<>();
        return lista;
    }
    
}
